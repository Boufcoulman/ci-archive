#!/bin/env bash
### Script for multi git project file editing

### Usage: bash ci_multi_edit.sh

SCRIPT_PATH=/home/beb/Documents/qirinus/devops/iqorchestra/microservices
TIMESTAMP="$(date +%Y%m%d-%H%M%S)"

function update_svc() {
    # update_svc service
    service="$1"

    if [[ "$service" == *"service" ]]; then
        basename "$service"
        git -C "$service" checkout develop || exit 1
        git -C "$service" pull || exit 1
        DIFF_OUT=$(diff build.gitlab-ci.yml "$service/.gitlab-ci.yml")
        if [[ -z $DIFF_OUT ]]; then
            echo "No difference for git project $(basename $service)"
            echo
            return 0
        else
            echo -e "$DIFF_OUT"
        fi
        while true; do
            read -r -p "Apply changes for $(basename $service) ? " yn
            case "$yn" in
                [Yy]* ) SKIP=false && break;;
                [Nn]* ) SKIP=true && break;;
                * ) echo "Please answer yes or no.";;
            esac
        done
        if "$SKIP"; then 
            echo
            return 1
        fi
        git -C "$service" switch -c "ci-cd/$TIMESTAMP" || exit 1
        cp build.gitlab-ci.yml "$service/.gitlab-ci.yml" || exit 1
        git -C "$service" add .gitlab-ci.yml || exit 1
        git -C "$service" commit -m ":construction_worker: ci-edit ${TIMESTAMP}" || exit 1
        git -C "$service" push --set-upstream origin "ci-cd/$TIMESTAMP" || exit 1
        git -C "$service" checkout develop || exit 1
    fi
}

for service in "${SCRIPT_PATH}"/*; do
    update_svc "$service"
done

for builder in "${SCRIPT_PATH}"/builders/*; do
    update_svc "$builder"
done
