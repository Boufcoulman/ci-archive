#!/bin/env bash
### Script allowing to retag every iqorchestra images
### Usage: bash global_retag.sh [CUSTOM_TAG]

set -e

# Take user input as tag or timestamp if not provided
CUSTOM_TAG="$1"
[[ -z ${CUSTOM_TAG} ]] && CUSTOM_TAG="auto-$(date +%Y%m%d-%H%M%S)"

SERVICES=('authentication-service/authent' 'authorization-service/author' 'builders/builder-outscale-service/bldr-outsc' 'builders/builder-scaleway-service/bldr-scway' 'connect-service/connect' 'design-service/design' 'frontend-service/frontend' 'library-service/library' 'notifications-service/notif' 'project-manager-service/prjct-mgmt' 'security-service/security' 'transform-service/transform' 'user-service/user')
REGISTRY_ENDPOINT="registry.gitlab.local/microservices"

for service in "${SERVICES[@]}"; do
    docker buildx imagetools create "${REGISTRY_ENDPOINT}/${service}:develop" --tag "${REGISTRY_ENDPOINT}/${service}:${CUSTOM_TAG}"
    docker buildx imagetools create "${REGISTRY_ENDPOINT}/${service}:develop" --tag "${REGISTRY_ENDPOINT}/${service}:latest"
done
